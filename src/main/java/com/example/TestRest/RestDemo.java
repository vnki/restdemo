package com.example.TestRest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dao.EmailDao;
import com.demo.dao.RestClientDao;
import com.demo.service.DemoService;
import com.demo.vo.Employee;
import com.demo.vo.Mailvo;
@Configuration
@RestController
@ComponentScan({"com.demo.dao","com.demo.service",} )
public class RestDemo {
	public RestDemo() {
		System.out.println("RestDemo.RestDemo()");
	}
	@Autowired
	private DemoService ser;
	@Autowired
	private EmailDao mail;
	@Autowired
	private RestClientDao restdao;
	@RequestMapping(path="/all",method = RequestMethod.GET,produces = { MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE})
	public List<Employee> getEmpData() {
		List<Employee> elist=ser.getData();
		for(Employee e:elist) {
			System.out.println(e);
		}
		return elist;
		
	}
	@RequestMapping(path="/add/{id}/{id1}",method = RequestMethod.GET)
	public int addFunctionality(@PathVariable int id, @PathVariable int id1) {
		
		return id+id1;
		
	}
	@RequestMapping(path="/mul/{id}/{id1}",method = RequestMethod.GET)
	public float mulFunctionality(@PathVariable float id, @PathVariable float id1) {
		
		return id+id1;
		
	}
	@RequestMapping(path="/supcid/{cid}",method = RequestMethod.GET)
	public String getSupervisorCid(@PathVariable String cid) {
		 Employee a=ser.getSupervData(cid);
		return a.getCid();
		
	}
	@PostMapping(path = "/in", produces = { MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE},consumes ={ MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE} )
	public Employee getpost(@RequestBody Employee emp) {
		System.out.println(emp);
		return emp;
	}
	@PostMapping(path = "/mail", produces = { MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE},consumes ={ MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE} )
	public Mailvo getmail(@RequestBody Mailvo emp) {
		System.out.println(emp);
		//"kotasainath13@gmail.com"
		mail.sendSimpleMessage(emp.getTo(), emp.getSubject(), emp.getBody());
		
		return emp;
	}
	@GetMapping("/rest")
	public List<Object> getRestTem(){
		return restdao.getEmploye();
		
	}
	@PostMapping(path = "/restcmail", produces = { MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE},consumes ={ MediaType.APPLICATION_XML_VALUE,MediaType.APPLICATION_JSON_VALUE} )
	public ResponseEntity<Mailvo> restcgetmail(@RequestBody Mailvo emp) {
		System.out.println(emp);
		//"kotasainath13@gmail.com"
		return restdao.getEmployes(emp);
		
		
	}
	
}
