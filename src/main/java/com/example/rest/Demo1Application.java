package com.example.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
// import com.demo.webconfig.WebConfigu;
import com.demo.dao.GetConConfig;
import com.demo.webconfig.MailConfig;
import com.demo.webconfig.RestTemplateConfig;
import com.example.TestRest.RestDemo;
import com.example.TestRest.RestSai;

@SpringBootApplication
 @Import(value = { RestDemo.class,GetConConfig.class,MailConfig.class,RestTemplateConfig.class})// ,WebConfigu.class
//@Import(value = { RestDemo.class,MailConfig.class,RestTemplateConfig.class})// ,WebConfigu.class
public class Demo1Application {
	public static void main(String[] args) {
		System.out.println("yes first");
		SpringApplication.run(Demo1Application.class, args);
	}
}
