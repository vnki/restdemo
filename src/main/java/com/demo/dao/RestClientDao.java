package com.demo.dao;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.demo.vo.Employee;
import com.demo.vo.Mailvo;
@Component
public class RestClientDao {
   @Autowired
private RestTemplate rt;
   public List<Object >getEmploye (){
	   String url="http://localhost:8089/all";
	   Object[] Objects=rt.getForObject(url, Object[].class);
	   return Arrays.asList(Objects);
   }
   public ResponseEntity<Mailvo>  getEmployes (Mailvo emp){
	   String url="http://localhost:8089/mail";
	  
	
	   
	  ResponseEntity<Mailvo> a= rt.postForEntity(url, emp, Mailvo.class);
	   return a;
   }
}
