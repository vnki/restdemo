package com.demo.dao;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class GetConConfig {
	public GetConConfig() {
		System.out.println("GetConConfig.GetConConfig()");
	}
	@Bean("pddb")
	public DataSource getDataSourceObject() {
		System.out.println("get data SourceObject");
	DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//		BasicDataSource dataSourceBuilder =new BasicDataSource();
       // dataSourceBuilder.driverClassName("com.ibm.db2.jcc.DB2Driver");
       // dataSourceBuilder.url("jdbc:db2://addcdb2p.dvdc.appl.chrysler.com:446/ADDCDB2P");
       // dataSourceBuilder.username("dgipacdm");
       // dataSourceBuilder.password("dgipacdm");
        dataSourceBuilder.driverClassName("com.ibm.db2.jcc.DB2Driver");
        dataSourceBuilder.url("jdbc:db2://srvr2653.dbms.chrysler.com:26530/AUCPPRPS");
        dataSourceBuilder.username("hrsync");
        dataSourceBuilder.password("devsync89");
       
        return dataSourceBuilder.build();
		
	}
	
	
	@Bean("psdbcon")
	public JdbcTemplate getTemplate() {
		JdbcTemplate jt=new JdbcTemplate(getDataSourceObject());
		return jt;
		
	}
	@Bean("pdmdb")
	public DataSource getPdmDataSourceObject() {
		System.out.println("get data SourceObject");
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("com.ibm.db2.jcc.DB2Driver");
        dataSourceBuilder.url("jdbc:db2://addcdb2p.dbms.chrysler.com:446/ADDCDB2P");
        dataSourceBuilder.username("dgipacdm");
        dataSourceBuilder.password("dgipacdm");
        
        return dataSourceBuilder.build();
		
	}
	
	
	@Bean("pdmdbcon")
	public JdbcTemplate getPDMTemplate() {
		JdbcTemplate jt=new JdbcTemplate(getPdmDataSourceObject());
		return jt;
		
	}
	
	
	

}
