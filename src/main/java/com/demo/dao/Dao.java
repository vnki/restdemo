package com.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.demo.vo.Employee;
@Repository
@Service
public class Dao {

	String SELECT_BY_ID_QUERY="SELECT A.EMPLID, A.EFFDT, A.EFFSEQ , A.CH_SUPV_ID FROM PSOFTUDB.PS_CH_RPTS_TO_TBL A WHERE A.EMPLID = ?  AND A.EFFDT = (select MAX(A2.EFFDT) from PSOFTUDB.PS_CH_RPTS_TO_TBL A2 WHERE A2.EMPLID = A.EMPLID)  AND A.EFFSEQ = (SELECT MAX(A3.EFFSEQ) FROM PSOFTUDB.PS_CH_RPTS_TO_TBL A3 WHERE A3.EMPLID = A.EMPLID  AND A3.EFFDT = A.EFFDT) WITH UR";
		String query="SELECT I_FIATID,N_PERSN_FIRST,N_LAST,D_BIRTH_MO_DAY FROM (SELECT ROW_NUMBER() OVER(ORDER BY N_PERSN_FIRST) AS rn, uf.* FROM U.UFIATID uf  where uf.C_REMD_STAT='M') as rn1  WHERE rn BETWEEN 1 AND 30";	
	String  a="select * from U.UFIATID";
	String sai="select A.I_PERSN_LOGON_ID,B.N_LAST,B.N_PERSN_FIRST  from U.UPRSNID A join U.UPERSN B on A.I_PERSN=B.I_PERSN where A.I_PERSN=1641351";

	@Qualifier("psdbcon")
	@Autowired
	private JdbcTemplate  psjt;
	
	
	@Qualifier("pdmdbcon")
	@Autowired
	private JdbcTemplate  pdmjt;
public Dao() {
	// TODO Auto-generated constructor stub
	System.out.println("Dao.Dao()");
}


	public Employee getSupCid(String empId) {
		
		return this.psjt.queryForObject(SELECT_BY_ID_QUERY, new EmployeeMapper(), 
	    empId);
	  }
	 private static final class EmployeeMapper implements RowMapper<Employee> {
		    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		      Employee emp = new Employee();
		      emp.setCid(rs.getString("CH_SUPV_ID"));
		      
		      return emp;
		    }
		  } 

	 public List<Employee> getAllEmployees(){ 
		
		 return pdmjt.query(query,new ResultSetExtractor<List<Employee>>(){  
		    @Override  
		     public List<Employee> extractData(ResultSet rs) throws SQLException,  
		            DataAccessException {  
		      
		        List<Employee> list=new ArrayList<Employee>();  
		        while(rs.next()){  
		        Employee e=new Employee();  
		        e.setCid(rs.getString("I_FIATID").trim());  
		        e.setFname(rs.getString("N_PERSN_FIRST").trim());  
		        e.setLname(rs.getString("N_LAST").trim());  
		        list.add(e);  
		        }  
		        return list;  
		        }  
		    });  
		  }  
	 
	 public List<Employee> findAll()
	    {
	       System.out.println("Dao.findAll()");
			try {
				m1(pdmjt.getDataSource().getConnection());
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        List<Map<String, Object>> list = pdmjt.queryForList(query);

	        List<Employee> employeeList = new ArrayList<Employee>();

	        for (Map<String, Object> map : list)
	        {
	           
	            /*employee.setEmployeeId((Long) map.get("EMPLOYEE_ID"));
	            employee.setName((String) map.get("NAME"));
	            employee.setAge((Integer) map.get("AGE"));
	            employee.setSalary((Integer) map.get("SALARY"));*/
	            Employee e=new Employee();  
		        e.setCid( (String)map.get("I_FIATID"));  
		        e.setFname((String) map.get("N_PERSN_FIRST") );  
		        e.setLname( (String) map.get("N_LAST") ); 
	            employeeList.add(e);
	        }
	        return employeeList;
	    }
	 public void m1(Connection con)  {
			ResultSet results = null;
			PreparedStatement statement = null;
			String b="SELECT I_PERSN,I_PERSN_LOGON_ID,I_FIATID FROM U.UFIATID";
			try {
				statement = con.prepareStatement(b);
			
			results = statement.executeQuery();
			while (results.next()) {
				System.out.println(results.getString(1)+" "+results.getString(2)+" "+results.getString(3) );
				
			}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				/*
				 * try { // con.close(); } catch (SQLException e) { // TODO Auto-generated catch
				 * block e.printStackTrace(); }
				 */}
		}

	 public List<Employee> getEmployes(){
		 String q="select * from PSOFTUDB.PS_CH_RPTS_TO_TBL fetch first 100 rows only";
		 
		 return psjt.query(q,new ResultSetExtractor<List<Employee>>(){  
		    @Override  
		     public List<Employee> extractData(ResultSet rs) throws SQLException,  
		            DataAccessException {  
		      
		        List<Employee> list=new ArrayList<Employee>();  
		        while(rs.next()){  
		        Employee e=new Employee();  
		        e.setCid(rs.getString("EMPLID"));  
		        e.setFname(rs.getString("CH_SUPV_ID"));  
		        e.setLname(rs.getString("CH_LOGON"));  
		        list.add(e);  
		        }  
		        return list;  
		        }  
		    });  
		  
		 
	 }
	 
	}
	 

