package com.demo.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Employee {
	
	public Employee() {
		System.out.println("Employee.Employee()");
	}
	private String cid;
	private String fname;
	private String lname;
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	@Override
	public String toString() {
		return "Employee [cid=" + cid + ", fname=" + fname + ", lname=" + lname + "]";
	}
	public void setLname(String lname) {
		this.lname = lname;
	}

}
