package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.Dao;
import com.demo.vo.Employee;
@Service
public class DemoService {
	public DemoService() {
		// TODO Auto-generated constructor stub
		System.out.println("DemoService.DemoService()");
	}
	@Autowired
	private Dao dao;
	public List<Employee> getData(){
		return dao.getAllEmployees();
	}
	public Employee getSupervData(String cid){
		return dao.getSupCid(cid);
	}
	
	}
